//******************************************************************************
//
//
//		タイトル処理
//
//
//******************************************************************************

#include "All.h"
#include "3DLib\Sprite3D.h"

//******************************************************************************
//
//		初期設定
//
//******************************************************************************

void SceneTitle::init(UINT i )
{
	timer = 0;
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			spr3D[0] = new Sprite3D(GetDevice(), L"DATA\\picture\\Title.png");
			spr3D[1] = new Sprite3D(GetDevice(), L"DATA\\picture\\Title.png");
			pMusic->musicPlay(ENUM::TITLE, true);
			break;
		default:
			break;
	}


}

//******************************************************************************
//
//		更新処理
//
//******************************************************************************

void SceneTitle::update(UINT i )
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			++timer;
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			break;
		default:
			break;
	}
	//セレクト画面へ
	if (input::TRG(0) & input::PAD_START || input::TRG(0) & input::PAD_TRG1 || input::TRG(0) & input::PAD_TRG2 || input::TRG(0) & input::PAD_TRG3) {
		//タイトルBGMを止める
		pMusic->musicStop(ENUM::TITLE);
		//タイトルから進むSE
		pMusic->soundPlay(12);
		setScene(pSceneSelect);

	}
	if (GetAsyncKeyState('Z') < 0 || GetAsyncKeyState('X') < 0 || GetAsyncKeyState('C') < 0 || GetAsyncKeyState(VK_SPACE) < 0)
	{
		//タイトルBGMを止める
		pMusic->musicStop(ENUM::TITLE);
		//タイトルから進むSE
		pMusic->soundPlay(12);
		setScene(pSceneSelect);
	}
	if(GetAsyncKeyState('Z')<0 || GetAsyncKeyState('X')<0 || GetAsyncKeyState('C')<0) setScene(pSceneSelect);
	//if (timer > SEC * 5)setScene(pSceneSelect);
	// 5秒後にデモへ切り換え
	//if (timer > 60 * 5)setScene(pSceneDemo);

}

//******************************************************************************
//
//		描画処理
//
//******************************************************************************
#include "Camera.h"
void SceneTitle::draw(UINT windowNum)
{
	switch (windowNum)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
		/*	spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1280, 760, static_cast<float>(WinFunc::GetScreenWidth(i)) / 1280, static_cast<float>(WinFunc::GetScreenHeight(i)) / 760);*/
			//spr[0]->Render2(GetDeviceContext(), WinFunc::GetCursorPos().x, WinFunc::GetCursorPos().y, 0, 0, 1280, 760, 1, 1);
			//spr[0]->Render3(GetDeviceContext(), WinFunc::GetCursorPos().x, WinFunc::GetCursorPos().y, WinFunc::GetCursorPos().x,0,0, 0, 1280, 760, 1, 1);
			{
				
				DirectX::XMMATRIX matrix = DirectX::XMMatrixIdentity();
				LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_2);
				LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_2);
				float aspect = (float)width / height;
				pCameraManager->SetPerspective(30 * 0.01745f, aspect, 0.1f, 100.0f);
				DirectX::XMMATRIX proj = pCameraManager->GetProjection();
				DirectX::XMMATRIX view = pCameraManager->GetView_();
				static float ang = 0;
				/*matrix *= DirectX::XMMatrixRotationX(ang);
				matrix *= DirectX::XMMatrixRotationY(ang);
				matrix *= DirectX::XMMatrixRotationZ(ang);
				matrix *= DirectX::XMMatrixTranslation(1, 1,ang);*/
				ang += 0.01f;

				//	Matrix -> Float4x4 変換
				DirectX::XMFLOAT4X4 world_view_projection;
				DirectX::XMFLOAT4X4 world_;
			DirectX::XMStoreFloat4x4(&world_, matrix);
				DirectX::XMStoreFloat4x4(&world_view_projection, matrix * proj * view);


				DirectX::XMFLOAT3 pos[4] = { DirectX::XMFLOAT3(100,100,0),DirectX::XMFLOAT3(500,100,0) ,DirectX::XMFLOAT3(100,500 / 3,0) ,DirectX::XMFLOAT3(500,500,0) };
				DirectX::XMFLOAT3 pos2[4] = { DirectX::XMFLOAT3(100,500 / 3,0),DirectX::XMFLOAT3(500,500,0) ,DirectX::XMFLOAT3(100,500,0) ,DirectX::XMFLOAT3(500,500,0) };
				//DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(pos[0].x, pos[0].y, 0);
#define XXX (0)
#define YYY (0)
				DirectX::XMFLOAT2 texPos[4] = { DirectX::XMFLOAT2(XXX,YYY),DirectX::XMFLOAT2(1280,YYY) ,DirectX::XMFLOAT2(XXX,760 / 3) ,DirectX::XMFLOAT2(1280,760) };
				DirectX::XMFLOAT2 texPos2[4] = { DirectX::XMFLOAT2(XXX,YYY + 760 / 3),DirectX::XMFLOAT2(1280,760) ,DirectX::XMFLOAT2(XXX,760) ,DirectX::XMFLOAT2(1280,760) };

				////行列反映
				for (int i = 0; i < 4; ++i)
				{
					pos[i].x *= -1;
					pos[i].y *= -1;
					pos[i].z *= -1;
					DirectX::XMVECTOR vecpos = { 0 };
					vecpos = DirectX::XMLoadFloat3(&pos[i]);
					vecpos = DirectX::XMVector3Transform(vecpos, matrix * proj * view);
					DirectX::XMStoreFloat3(&pos[i], vecpos);
				}
				//	pos2[i].x *= -1;
				//	pos2[i].y *= -1;
				//	pos2[i].z *= -1;
				//	DirectX::XMVECTOR vecpos2 = { 0 };
				//	vecpos2 = DirectX::XMLoadFloat3(&pos2[i]);
				//	vecpos2 = DirectX::XMVector3Transform(vecpos2, matrix * proj * view);
				//	DirectX::XMStoreFloat3(&pos2[i], vecpos2);
				//}

				
				//DirectX::XMVECTOR verpos[4];//XMVECTOR
				//for (int i = 0; i < 4; i++)
				//{
				//	//verpos[i] = DirectX::XMLoadFloat2(&pos[i]);
				//	matrix*=proj*view * DirectX::XMMatrixTranslation(texPos[i].x, texPos[i].y, 0);
				//}
				//
				//verpos[i] = world_view_projection;
				//DirectX::XMStoreFloat4x4(&world_, matrix);

				spr3D[0]->RenderSprite3D(GetDeviceContext(), world_view_projection, world_, pos, texPos);
				spr3D[1]->RenderSprite3D(GetDeviceContext(), world_view_projection, world_, pos2, texPos2);
			}
			break;
		default:
			break;
	}
}

//******************************************************************************