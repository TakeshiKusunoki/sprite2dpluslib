#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <stdio.h>
#include <time.h>
#include <Mmreg.h>
#include "Template.h"

#pragma comment(lib, "winmm.lib")
//
// 型と構造体の定義
//
typedef unsigned char      byte;
typedef unsigned int       uint32;

// 再生用バッファの関連定数
#define BUFFER_LEN 44100
#define BUFFER_NUM 2

class MusicFunc : public Singleton<MusicFunc>
{
private:
	// Waveファイルのチャンク情報を格納するための型
	typedef struct
	{
		uint32 tag;
		uint32 size;
	} WaveChunk;

	// RIFFチャンクは通常のチャンク情報にフォーマット情報がつく
	typedef struct
	{
		uint32 tag;
		uint32 size;
		uint32 format;
	}RIFFHeader;


private:
	int state;
	int timer;

	// WAVEファイルのフォーマット
	WAVEFORMATEX* wfe;

	// オーディオデバイスのハンドル
	HWAVEOUT* hWaveOut;

	// バッファ
	WAVEHDR** whdr;

	// ファイルポインタ
	FILE** fp;

	//ファイル名
	char** fileNames;

	UINT DATA_NUM;
public:
	MusicFunc() {
		fp = nullptr;
		DATA_NUM = 0;
	};
	~MusicFunc() {
		if (fp)delete[] fp;
		if (hWaveOut)delete[] hWaveOut;
		if (wfe)delete[] wfe;
		for (size_t i = 0; i < DATA_NUM; i++)
		{
			if (whdr[i])delete[] whdr[i];
			whdr[i] = nullptr;
		}
		if (whdr)delete[] whdr;
	};
	//filenameは最後にnullptrいれる
	bool WaveLoad(char * filename[]);
	void WavePlay(UINT i);
	bool WaveClose(UINT i);
};
#define pMusic  (MusicFunc::getInstance())



